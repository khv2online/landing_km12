import { TimelineMax } from 'gsap';
import scrollMagic from 'scrollmagic';
import 'scrollGsap';
import $ from 'jquery';

export default function animation() {
  $(() => {
    const controller = new scrollMagic.Controller();
    const animationsElement = [].slice.call(document.querySelectorAll('.animation'));
    let delay = 0;
    animationsElement.forEach((item) => {
      const el = item;
      const position = el.getBoundingClientRect();
      position.absoluteTop = window.scrollY + position.top;
      const timeline = new TimelineMax();
      if (position.absoluteTop < window.innerHeight) {
        timeline.delay(delay);
      }
      if (el.classList.contains('animation--slide-up')) {
        if (el.classList.contains('animation--stagger')) {
          timeline.staggerTo(el.children, 0.3, { top: 0, opacity: 1 }, 0.1);
        } else {
          timeline.to(el, 0.3, { top: 0, opacity: 1 });
        }
      } else if (el.classList.contains('animation--slide-up-opacity')) {
        if (el.classList.contains('animation--stagger')) {
          timeline.staggerTo(el.children, 0.3, { top: 0, opacity: 1 }, 0.1);
        } else {
          timeline.to(el, 0.3, { top: 0, opacity: 1 });
          timeline.to(el.children, 0.3, { opacity: 1 });
        }
      } else if (el.classList.contains('animation--slide-up-text')) {
        if (el.classList.contains('animation--stagger')) {
          [].slice.call(el.children).forEach((subEl) => {
            const subItem = subEl;
            timeline.to(subItem, 0.5, {
              left: 0,
              opacity: 1,
            });
          });
          el.classList.add('animate--init');
        } else {
          el.classList.add('animate--init');
          timeline.to(el, 0.5, {
            left: 0,
            opacity: 1,
          });
        }
      } else if (el.classList.contains('animation--slide-right-opacity')) {
        timeline.to({ tmp: 0 }, 0.8, { tmp: 1 });
        timeline.to(el, 0.3, { x: 0, opacity: 1 });
        delay -= 0.8;
      }
      delay += timeline.duration() - 0.2;
      if (position.absoluteTop >= window.innerHeight) {
        new scrollMagic.Scene({
          triggerElement: el,
          triggerHook: 1,
          reverse: false,
          loglevel: 0,
        }).setTween(timeline).addTo(controller);
      }
    });
  });
}

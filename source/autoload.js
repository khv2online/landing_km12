import 'svgxuse';

import BannerScroll from './blocks/dev/banner';
import Build from './blocks/dev/build';
import Banner from './blocks/dev/visual';
import Place from './blocks/dev/place';
import animation from './base/scripts/animation';

require('./autoload.scss');

animation();
new BannerScroll();
new Build();
new Banner();
new Place();

import TweenLite from 'gsap';

require('gsap/ScrollToPlugin');

class BannerScroll {
  constructor() {
    const btn = document.querySelector('.dev-banner__scroll');

    if (btn) {
      btn.addEventListener('click', (event) => {
        event.preventDefault();
        const block = document.querySelector(btn.getAttribute('href'));
        const scroll = window.pageYOffset || document.documentElement.scrollTop;
        const point = block.getBoundingClientRect().top + (scroll - 76);

        TweenLite.to(window, 2, { scrollTo: point });
      });
    }
  }
}
export default BannerScroll;

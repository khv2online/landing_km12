import $ from 'jquery';

require('@fancyapps/fancybox');

class Place {
  constructor() {
    $('.dev-place__img').fancybox({
      loop: true,
      margin: [8, 0],
      clickContent(current) {
        return current.type === 'image' ? 'next' : false;
      },
      baseClass: 'fullscreen',
      baseTpl:
      '<div class="fancybox-container" role="dialog" tabindex="-1">' +
      '<div class="fancybox-bg"></div>' +
      '<div class="fancybox-inner">' +
      '<div class="fancybox-toolbar">{{buttons}}</div>' +
      '<div class="fancybox-stage"></div>' +
      '<div class="fancybox-caption-wrap">' +
      '<div class="fancybox-caption"></div>' +
      '<div class="fancybox-navigation">{{arrows}}</div>' +
      '</div>' +
      '</div>' +
      '</div>',
    });
  }
}
export default Place;

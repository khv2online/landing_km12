import { TweenMax, SlowMo } from 'gsap';

class AmimatedDotted {
  constructor(imgData, containerEl) {
    this.clientWidth = window.innerWidth;
    this.runAnimated = true;
    this.container = containerEl;
    this.imgData = imgData;
    this.img = null;
    this.SPACING = 3;
    this.ROWS = 620 / this.SPACING;
    this.COLS = 1500 / this.SPACING;
    this.THICKNESS = 80 ** 2;
    this.MARGIN = 0;
    this.COLOR = 255;
    this.DRAG = 0.95;
    this.EASE = 0.25;
    this.list = null;
    this.man = true;
    this.tog = true;
    this.mx = null;
    this.my = null;
    this.d = null;
    this.a = null;
    this.b = null;
    this.w = null;
    this.h = null;
    this.particle = {
      vx: 0,
      vy: 0,
      x: 0,
      y: 0,
    };
    this.NUM_PARTICLES = this.ROWS * this.COLS;
    this.back = document.createElement('canvas');
    this.backCtx = this.back.getContext('2d');
    this.distinctData = null;
    this.container.parentElement.position = this.container.parentElement.getBoundingClientRect();
    this.container.parentElement.position.topCalc = this.container.parentElement.position.top +
      window.scrollY;
    window.addEventListener('scroll', this.checkRunAnimated.bind(this));
    this.init();
    this.resize();
    this.runAnimated = false;
    this.checkRunAnimated();
  }

  checkRunAnimated() {
    const position = this.container.parentElement.position.topCalc;
    if ((window.scrollY >= position - window.innerHeight) &&
      (window.scrollY <= position + this.container.parentElement.position.height)) {
      this.runAnimated = true;
    } else {
      this.runAnimated = false;
    }
  }

  resize() {
    window.blickX = 0;
    this.clientWidth = window.innerWidth;
    const { parentElement } = this.container;
    this.getImage(this.imgData, 0, 0, parentElement.clientWidth, parentElement.clientHeight, () => {
      this.backCtx.clearRect(0, 0, this.back.width, this.back.height);
      this.backCtx.drawImage(this.img, 0, 0);
      this.distinctData = this.backCtx.getImageData(0, 0, this.back.width, this.back.height);
      this.imagesLoad = true;
      TweenMax.to({
        blick: 0,
      }, 8, {
        blick: this.distinctData.width,
        repeat: -1,
        onUpdate(tween) {
          window.blickX = parseInt(tween.target.blick, 10);
          this.draw();
        },
        onUpdateParams: ['{self}'],
        onUpdateScope: this,
        ease: SlowMo.ease.config(0.1, 0.1, false),
      });
      this.draw();
    });
  }

  getImage(svgData, dx, dy, dw, dh, fn) {
    let svg = svgData;
    let image = null;
    const svgDocument = window.$.parseXML(svg);
    const svgElement = window.$(svgDocument).find('svg');
    const svgHeight = svgElement.attr('height');
    const svgWidth = svgElement.attr('width');
    const scaleX = dw / svgWidth;
    const scaleY = dh / svgHeight;
    let transformTag = null;
    transformTag = window.$(document.createElementNS('http://www.w3.org/2000/svg', 'g'))
      .attr('transform', `scale(${scaleX},${scaleY})`);
    svgElement.attr({
      height: dh,
      viewbox: `0 0 ${dw} ${dh}`,
      width: dw,
    }).wrapInner(transformTag);
    svg = (new window.XMLSerializer()).serializeToString(svgElement[0]);
    image = window.$('<img/>', {
      height: dh,
      width: dw,
      src: `data:image/svg+xml,${window.escape(svg)}`,
    });
    image.on('load', () => {
      [this.img] = image;
      if (fn !== undefined) {
        fn(dw, dh);
      }
    });
  }

  init() {
    const canvas = document.createElement('canvas');
    this.ctx2 = canvas.getContext('2d');
    canvas.width = (this.COLS * this.SPACING) + (this.MARGIN * 2);
    this.w = canvas.width;
    canvas.height = (this.ROWS * this.SPACING) + (this.MARGIN * 4);
    this.h = canvas.height;
    this.back.height = canvas.height;
    this.back.width = canvas.width;
    this.container.appendChild(canvas);
  }
  draw() {
    this.ctx2.clearRect(0, 0, this.w, this.h);
    if (this.distinctData !== null) {
      const a = this.ctx2.createImageData(this.w, this.h);
      const b = a.data;
      let y;
      const width = 25;
      for (y = 0; y < this.distinctData.height; y += 1) {
        for (let x = window.blickX; x < window.blickX + width; x += 1) {
          const n = (x + (y * this.distinctData.width)) * 4;
          b[n] = this.COLOR;
          b[n + 1] = b[n];
          b[n + 2] = b[n];
          const realIndex = (x - window.blickX);
          b[n + 3] = (((width / 2) - Math.abs((width / 2) - realIndex)) * 255) / (width / 2);
        }
      }
      if (this.distinctData !== null) {
        for (let i = 0; i < this.distinctData.data.length; i += 4) {
          b[i + 3] = (this.distinctData.data[i + 3] !== 255) ? 0 : b[i + 3];
        }
      }
      this.ctx2.putImageData(a, 0, 0);
    }
  }
}


class Banner {
  constructor() {
    const container = document.getElementById('container');
    const dataSvg = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg width="184" height="60" xmlns="http://www.w3.org/2000/svg">
    <g fill-rule="nonzero" fill="none">
    <path d="M138.003 43.92v15.406h-36.484v-6.01c6.345-5.954 22.645-19.61 24.942-27.204.82-2.731 1.368-5.408 1.368-9.177 0-6.61-3.392-13.712-10.885-13.712-4.814 0-10.667 3.605-10.667 8.85 0 .546.438 1.639 2.407 1.092.657-.164 1.313-.382 1.75-.382 2.954 0 5.088 1.366 5.088 4.589 0 3.66-2.188 5.353-5.69 5.353-4.156 0-6.344-3.77-6.344-7.538C103.488 5.736 112.24 0 121.1 0c9.19 0 17.777 5.517 17.777 15.46 0 5.08-1.641 9.56-9.299 16.279l-21.825 19.229h22.974c4.32 0 6.4-.273 6.4-7.047h.875zM72.025 7.015v17.809h9.982V0c-4.553 1.187-10.805 1.78-16.18 1.78h-5.375v1.458h5.156c5.594.054 6.417-.162 6.417 3.777zM81.968 42.81H72.11v15.02H61.163v1.507h31.48V57.83H81.969z" fill="#555555"/>
    </g>
    </svg>`;
    if (container !== null) {
      const animation = new AmimatedDotted(dataSvg, container);
      window.addEventListener('resize', () => {
        animation.resize();
      });
    }
  }
}
export default Banner;

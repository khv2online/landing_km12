import $ from 'jquery';
import Swiper from 'swiper';

window.jQuery = $;
window.$ = $;

require('@fancyapps/fancybox');

class Build {
  constructor() {
    setTimeout(() => {
      new Swiper('.dev-build__slider', {
        slidesPerView: 'auto',
        scrollbar: {
          el: '.dev-build__scrollbar',
          hide: true,
        },
        spead: 500,
        loop: false,
      });
    }, 200);

    $('.dev-build__slider--detail .dev-build__slide').fancybox({
      loop: true,
      margin: [8, 0],
      clickContent(current) {
        return current.type === 'image' ? 'next' : false;
      },
      caption() {
        const caption = `${$(this).find('.dev-build__description').html()}`;

        return caption;
      },
      baseTpl:
            '<div class="fancybox-container" role="dialog" tabindex="-1">' +
                '<div class="fancybox-bg"></div>' +
                '<div class="fancybox-inner">' +
                    '<div class="fancybox-toolbar">{{buttons}}</div>' +
                    '<div class="fancybox-stage"></div>' +
                    '<div class="fancybox-caption-wrap">' +
                      '<div class="fancybox-caption"></div>' +
                      '<div class="fancybox-navigation">{{arrows}}</div>' +
                    '</div>' +
                '</div>' +
            '</div>',
    });
  }
}
export default Build;
